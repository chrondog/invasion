AddCSLuaFile()

ENT.Base 			= "base_nextbot"
ENT.Spawnable		= true

function ENT:Initialize( )
	self:SetModel( "models/dog.mdl" )
	
	self.LoseTargetDist	= 5000
	self.SearchRadius 	= 3000
	
	self:SetMaxHealth( 100 )
	self:SetHealth( 100 );
	
	self:SetCollisionBounds( Vector( -8, -8, 0 ), Vector(8, 8, 128 ) ) 
end

function ENT:SetEnemy( ent )
	self.Enemy = ent
end

function ENT:GetEnemy( )
	return self.Enemy
end

function ENT:HaveEnemy( )
	if ( self:GetEnemy( ) and IsValid( self:GetEnemy( ) ) ) then
		if ( self:GetRangeTo( self:GetEnemy( ):GetPos( ) ) > self.LoseTargetDist ) then
			return self:FindEnemy( )
		elseif ( self:GetEnemy( ):IsPlayer( ) and !self:GetEnemy( ):Alive( ) ) then
			return self:FindEnemy( )
		end	
		return true
	else
		return self:FindEnemy( )
	end
end

function ENT:FindEnemy( )
	local _ents = ents.FindInSphere( self:GetPos(), self.SearchRadius )
	for k,v in pairs( _ents ) do
		if ( v:IsPlayer() and v:Alive()) then
			self:SetEnemy( v )
			return true
		end
	end	
	self:SetEnemy( nil )
	return false
end

function ENT:FaceEnemy()
	local canSee = false;
	
	while (canSee == false) do
		self.loco:FaceTowards( self:GetEnemy():GetPos() )
		local dir = ( self:GetEnemy():GetPos() - self:GetPos() ):GetNormal(); -- replace with eyepos if you want
		canSee = dir:Dot( self:GetForward() ) > 0.98; -- -1 is directly opposite, 1 is self:GetForward(), 0 is orthogonal
		
		coroutine.yield( )
	end
end

function ENT:OnKilled( dmginfo )
    hook.Call("OnNPCKilled", GAMEMODE, self, dmginfo:GetAttacker(), dmginfo:GetInflictor() )
	self:Remove()
	timer.Stop("Attack")
end

function ENT:RunBehaviour( )
	while ( true ) do
		if ( self:HaveEnemy( ) ) then
			//self.loco:FaceTowards(self:GetEnemy():GetPos())
			self:FaceEnemy( )
			if( self:GetPos():Distance(self:GetEnemy():GetPos()) < 100 ) then
				timer.Create( "Attack", 1.2, 1, function() self:Attack() end)
				self:PlaySequenceAndWait( "pound" )
			else
				self:StartActivity( ACT_RUN )
				self.loco:SetDesiredSpeed( 450 )
				self.loco:SetAcceleration( 900 )
				self:ChaseEnemy( )
				self.loco:SetAcceleration( 400 )
			end
			
			self:StartActivity( ACT_IDLE )
		else
			self:StartActivity( ACT_WALK )
			self.loco:SetDesiredSpeed( 200 )
			self:MoveToPos( self:GetPos() + Vector( math.Rand( -1000, 1000 ), math.Rand( -1000, 1000 ), 0 ) )
			self:StartActivity( ACT_IDLE )
		end
		coroutine.yield()
	end
end

function ENT:Attack( )
	local vec = self:GetAngles( ):Forward( )
	local pos = self:GetPos( )
	
	pos = pos + ( vec * 50 )
	
	local _ents = ents.FindInSphere( pos, 50 )
	for k, v in pairs( _ents ) do
		if ( v:IsPlayer( ) ) then
			v:TakeDamage( 200, self, self )
		end
	end
end


opts = 
{ 
	lookahead = 300,
	tolerance = 45,
	draw = true,
	maxage = 0.1,
	repath = 0.1
}

function ENT:ChaseEnemy( options )
	local options = options or {}

	local path = Path( "Follow" )
	path:SetMinLookAheadDistance( options.lookahead or 300 )
	path:SetGoalTolerance( options.tolerance or 20 )
	path:Compute( self, self:GetEnemy():GetPos() )

	if ( !path:IsValid( ) ) then 
		return "failed" 
	end

	while ( path:IsValid( ) and self:HaveEnemy( ) ) do
		if ( path:GetAge( ) > 0.1 ) then
			path:Compute( self, self:GetEnemy( ):GetPos( ) )
		end
		path:Update( self )
		
		if( self:GetPos( ):Distance( self:GetEnemy( ):GetPos( ) ) < 100 ) then
			return "ok"
		end
		
		if ( options.draw ) then 
			path:Draw( ) 
		end
		
		if ( self.loco:IsStuck( ) ) then
			self:HandleStuck( )
			return "stuck"
		end

		coroutine.yield( )

	end

	return "ok"
end

list.Set( "NPC", "npc_dog_inv", {
	Name = "Dog",
	Class = "npc_dog_inv",
	Category = "Invasion"
} )