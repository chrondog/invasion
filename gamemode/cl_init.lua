include("shared.lua")

surface.CreateFont("SPF_N", {
        size = 16,
        weight = 400,
        antialias = false,
        shadow = true,
        font = "Verdana"})

SKILL_BASIC_WEAPONSPEED  = 1
SKILL_BASIC_HEALTHBONUS  = 2
SKILL_BASIC_DAMAGEREDUC  = 3
SKILL_BASIC_DAMAGEBONUS  = 4
SKILL_BASIC_REGENERATION = 5

SKILL_ACTIVE_SENTRY     = 1
SKILL_ACTIVE_RESURRECT  = 2
SKILL_ACTIVE_VORTEX     = 3
SKILL_ACTIVE_LIGHTNING  = 4
SKILL_ACTIVE_MAGICMAKER = 5

SKILL_PASSIVE_JUMPHEIGHT  = 1
SKILL_PASSIVE_VAMPIRISM   = 2
SKILL_PASSIVE_IRONLEGS    = 3
SKILL_PASSIVE_QUICKFOOT   = 4
SKILL_PASSIVE_RESUPPLY    = 5
SKILL_PASSIVE_AUTOHOP     = 6
SKILL_PASSIVE_REDEMPTION  = 7
SKILL_PASSIVE_ULTIMA      = 8
SKILL_PASSIVE_EQUILIBRIUM = 9

g_SkillList =
{	
	["basic"] =
	{
		[SKILL_BASIC_WEAPONSPEED] =
		{
			["name"]        = "Weapon Speed",
			["description"] = "Increases your weapon speed by 1% for every stat put in.",
			["stats"]       = 0
		},
		[SKILL_BASIC_HEALTHBONUS] =
		{
			["name"]        = "Health Bonus",
			["description"] = "Increases your max health by 1 for every stat put in.",
			["stats"]       = 0
		},
		[SKILL_BASIC_DAMAGEREDUC] =
		{
			["name"]        = "Damage Reduction",
			["description"] = "Increases your damage reduction by 0.5% for every stat put in.",
			["stats"]       = 0
		},
		[SKILL_BASIC_DAMAGEBONUS] =
		{
			["name"]        = "Damage Bonus",
			["description"] = "Increases your damage by 0.5% for every stat put in.",
			["stats"]       = 0
		},
		[SKILL_BASIC_REGENERATION] =
		{
			["name"]        = "Regeneration",
			["description"] = "Increases your health by 1 every 5 seconds for every stat put in.",
			["stats"]       = 0
		}
	},
	["active"] =
	{
		[SKILL_ACTIVE_SENTRY] =
		{
			["name"]  = "Sentry",
			["level"] = 0,
			[1] =
			{
				["description"] = "Spawns a sentry that shoots mobs for you. HP: 150, DB: 0%.",
				["cost"]        = 100
			},
			[2] =
			{
				["description"] = "Spawns a sentry that shoots mobs for you. HP: 370, DB: 100%.",
				["cost"]        = 400
			},
			[3] =
			{
				["description"] = "Spawn up to 2 sentries that shoot mobs for you. HP: 1000, DB: 300%.",
				["cost"]        = 1000
			},
			[4] =
			{
				["description"] = "Spawn up to 2 sentries that shoot mobs for you and also do AoE damage. HP: 1400, DB: 500%.",
				["cost"]        = 5000
			}
		},
		[SKILL_ACTIVE_RESURRECT] =
		{
			["name"]  = "Resurrect",
			["level"] = 0,
			[1] =
			{
				["description"] = "Allows you to resurrect another player (once per round).",
				["cost"]        = 100
			}
		},
		[SKILL_ACTIVE_VORTEX] =
		{
			["name"]  = "Vortex",
			["level"] = 0,
			[1] =
			{
				["description"] = "Creates a vortex where you're looking.",
				["cost"]        = 100
			}
		},
		[SKILL_ACTIVE_LIGHTNING] =
		{
			["name"]  = "Lightning Rod",
			["level"] = 0,
			[1] =
			{
				["description"] = "You body becomes a lightning rod and shocks nearby mobs.",
				["cost"]        = 100
			}
		},
		[SKILL_ACTIVE_MAGICMAKER] =
		{
			["name"]  = "Magic Weapon Maker",
			["level"] = 0,
			[1] =
			{
				["description"] = "You can give weapons magic properties.",
				["cost"]        = 100
			}
		}
	},
	["passive"] =
	{
		[SKILL_PASSIVE_JUMPHEIGHT] =
		{
			["name"]  = "Jump Height",
			["level"] = 0,
			[1] =
			{
				["description"] = "Increases your jump height by 100%.",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "Increases your jump height by 100% and allows you to double jump.",
				["cost"]        = 100
			},
			[3] =
			{
				["description"] = "Increases your jump height by 200% and allows you to double jump.",
				["cost"]        = 250
			},
			[4] =
			{
				["description"] = "Increases your jump height by 200% and allows you to jump infinitely.",
				["cost"]        = 1000
			}
		},
		[SKILL_PASSIVE_VAMPIRISM] =
		{
			["name"]  = "Vampirism",
			["level"] = 0,
			[1] =
			{
				["description"] = "5% of the damage you cause is given back to you in health.",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "10% of the damage you cause is given back to you in health.",
				["cost"]        = 250
			},
			[3] =
			{
				["description"] = "30% of the damage you cause is given back to you in health.",
				["cost"]        = 900
			},
			[4] =
			{
				["description"] = "60% of the damage you cause is given back to you in health.",
				["cost"]        = 1500
			},
			[5] =
			{
				["description"] = "100% of the damage you cause is given back to you in health.",
				["cost"]        = 5000
			}
			
		},
		[SKILL_PASSIVE_IRONLEGS] =
		{
			["name"]  = "Iron Legs",
			["level"] = 0,
			[1] =
			{
				["description"] = "Reduces fall damage by 10%",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "Reduces fall damage by 20%",
				["cost"]        = 60
			},
			[3] =
			{
				["description"] = "Reduces fall damage by 40%",
				["cost"]        = 135
			},
			[4] =
			{
				["description"] = "Reducdes fall damage by 80%",
				["cost"]        = 300
			},
			[5] =
			{
				["description"] = "Reduces fall damage by 100%",
				["cost"]        = 1000
			}
		},
		[SKILL_PASSIVE_QUICKFOOT] =
		{
			["name"]  = "Quickfoot",
			["level"] = 0,
			[1] =
			{
				["description"] = "Increases your movement speed by 5%",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "Increases your movement speed by 10%",
				["cost"]        = 100
			},
			[3] =
			{
				["description"] = "Increases your movement speed by 20%",
				["cost"]        = 200
			},
			[4] =
			{
				["description"] = "Increases your movement speed by 50%",
				["cost"]        = 500
			},
			[5] =
			{
				["description"] = "Increases your movement speed by 100%",
				["cost"]        = 1000
			}
		},
		[SKILL_PASSIVE_RESUPPLY] =
		{
			["name"]  = "Resupply",
			["level"] = 0,
			[1] =
			{
				["description"] = "Regenerates 1 ammo per second.",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "Regenerates 2 ammo per second.",
				["cost"]        = 50
			},
			[3] =
			{
				["description"] = "Regenerates 4 ammo per second.",
				["cost"]        = 100
			},
			[4] =
			{
				["description"] = "Regenerates 8 ammo per second.",
				["cost"]        = 300
			},
			[5] =
			{
				["description"] = "You gain infinite ammo.",
				["cost"]        = 2000
			}
		},
		[SKILL_PASSIVE_AUTOHOP] =
		{
			["name"]  = "Autobhop",
			["level"] = 0,
			[1] =
			{
				["description"] = "You can gain perfect jumps by holding your jump button down.",
				["cost"]        = 500
			}
		},
		[SKILL_PASSIVE_REDEMPTION] =
		{
			["name"]  = "Redemption",
			["level"] = 0,
			[1] =
			{
				["description"] = "You are granted a second chance at life per wave.",
				["cost"]        = 500
			}
		},
		[SKILL_PASSIVE_ULTIMA] =
		{
			["name"]  = "Ultima",
			["level"] = 0,
			[1] =
			{
				["description"] = "Causes a big explosion upon death.",
				["cost"]        = 500
			}
		},
		[SKILL_PASSIVE_EQUILIBRIUM] =
		{
			["name"]  = "Equilibrium",
			["level"] = 0,
			[1] =
			{
				["description"] = "Deals 5% of the damage to enemies that inflict damage on you.",
				["cost"]        = 25
			},
			[2] =
			{
				["description"] = "Deals 10% of the damage to enemies that inflict damage on you.",
				["cost"]        = 100
			},
			[3] =
			{
				["description"] = "Deals 30% of the damage to enemies that inflict damage on you.",
				["cost"]        = 300
			},
			[4] =
			{
				["description"] = "Deals 50% of the damage to enemies that inflict damage on you.",
				["cost"]        = 1000
			}
		}
	}
}


net.Receive("PlayerSpawned", function()
	if(g_ShowingAvatar == false and g_SteamID != nil) then
		g_Avatar = vgui.Create( "AvatarImage" )--, Panel )
		g_Avatar:SetSize( 64, 64 )
		g_Avatar:SetPos( 10, 8 )
		g_Avatar:SetSteamID( g_SteamID, 64 )
		
		g_ShowingAvatar = true
	end
end)

net.Receive("PlayerDied", function(ply)
	if(g_ShowingAvatar == true) then
		g_Avatar:Remove();
		g_ShowingAvatar = false
	end
end)


net.Receive("PlayerAuthed", function(ply)
	g_SteamID = net.ReadString()
	
	if(LocalPlayer():Alive()) then
		g_Avatar = vgui.Create( "AvatarImage")--, Panel )
		g_Avatar:SetSize( 64, 64 )
		g_Avatar:SetPos( 10, 8 )
		g_Avatar:SetSteamID( g_SteamID, 64 )
		
		g_ShowingAvatar = true
	end
end)

function GM:OnPlayerChat( ply, text, teamChat, isDead )
	if(text == "!level") then
		OpenStatsMenu()
	end
end

net.Receive("LevelUp", function(ply)
	local category = net.ReadString()
	local skill    = net.ReadInt(32)
	local stats    = net.ReadInt(32)
	
	g_SkillList[category][skill]["stats"] = stats
end)

function OpenStatsMenu()
	// Create main frame
	local Frame = CreateFrame()
	
	// Create right side information
	local Description = CreateDescription(Frame)
	
	// Create main property sheet
	local PropertySheet = CreatePropertySheet(Frame)
	
	// Create the basic tab of the property sheet
	local BasicTab = CreateBasicTab(PropertySheet)
	
	// Create the passive list for the property sheet
	local BasicList = CreateBasicList(BasicTab, Description)
	
	// Create the passive tab for the property sheet
	local PassiveTab = CreatePassiveTab(PropertySheet)
	
	// Create the passive list for the property sheet
	local PassiveList = CreatePassiveList(PassiveTab, Description)
	
	// Create the active tab for the property sheet
	local ActiveTab = CreateActiveTab(PropertySheet)
	
	// Create the active list for the property sheet
	local ActiveList = CreateActiveList(ActiveTab, Description)
	
	PropertySheet:AddSheet( "Basic", BasicTab, nil, false, false, "Upgrades that all classes have" )
	PropertySheet:AddSheet( "Passive", PassiveTab, nil, false, false, "Passive upgrades for your class" )
	PropertySheet:AddSheet( "Active", ActiveTab, nil, false, false, "Active upgrades for your class" )
	PropertySheet:SizeToContentWidth()
	
	// Create the 'Okay' button
	local Button = vgui.Create( "Button", Frame );
	Button:SetText( "Okay" );
	Button:SetWide( 100 );
	Button:SetPos( Frame:GetWide() - Button:GetWide() - 15, Frame:GetTall() - Button:GetTall() - 15 );
	function Button:DoClick( )
		Frame:Close( )
	end
end

function CreateFrame()
	local Frame = vgui.Create( "DFrame" )
	Frame:SetSize( 650, 310 ); //Set the size to 200x200
	Frame:Center()
	Frame:SetVisible( true );  //Visible
	Frame:MakePopup( ); //Make the frame
	Frame:SetTitle( "Stats menu" )
	
	return Frame
end

function CreateDescription(parent)
	local Description = vgui.Create( "DLabel", parent )
	Description:SetText( g_SkillList["basic"][SKILL_BASIC_WEAPONSPEED]["description"] )
	Description:SetPos( 330, 10 )
	Description:SetSize( 300, 100 )
	Description:SetWrap( true )
	Description:SetExpensiveShadow(1, Color(0, 0, 0, 255))
	
	return Description
end

function CreatePropertySheet(parent)
	local PropertySheet = vgui.Create( "DPropertySheet", parent )
	PropertySheet:SetPos( 5, 27 )
	PropertySheet:SetSize( parent:GetWide() - 10, parent:GetTall() - 32 )
	
	return PropertySheet
end

function CreatePassiveTab(parent)
	local PassiveTab = vgui.Create( "DPanelList", parent )
	PassiveTab:SetPos( 25,25 )
	PassiveTab:SetSize( 300, 200 )
	PassiveTab:SetSpacing( 5 ) -- Spacing between items
	PassiveTab:EnableHorizontal( false ) -- Only vertical items
	PassiveTab:EnableVerticalScrollbar( true ) -- Allow scrollbar if you exceed the Y axis
	
	return PassiveTab
end

function CreatePassiveList(parent, description)
	local PassiveList = vgui.Create( "DListView", parent )
	PassiveList:SetSize(600, 300)
	PassiveList:SetMultiSelect( false )
	PassiveList:AddColumn( "Skill" )
	PassiveList:AddColumn( "Current Level" )
	PassiveList:AddColumn( "Upgrade Cost" )
	
	// Add skills to list
	for k, _ in ipairs(g_SkillList["passive"]) do
		local cost = "Maxed"
		
		if(g_SkillList["passive"][k]["level"] < #g_SkillList["passive"][k]) then
			cost = g_SkillList["passive"][k][g_SkillList["passive"][k]["level"] + 1]["cost"]
		end
		
		PassiveList:AddLine( g_SkillList["passive"][k]["name"], g_SkillList["passive"][k]["level"], tostring(cost) )
	end
	
	PassiveList:SelectFirstItem()
	
	PassiveList.OnClickLine = function(parent, line, isselected)
		local sDesc = "Maxed"
		
		if(g_SkillList["passive"][line:GetID()]["level"] < #g_SkillList["passive"][line:GetID()]) then
			sDesc = "Next level: "..g_SkillList["passive"][line:GetID()][g_SkillList["passive"][line:GetID()]["level"] + 1]["description"]
		else
			sDesc = "Maxed out: "..g_SkillList["passive"][line:GetID()][g_SkillList["passive"][line:GetID()]["level"]]["description"]
		end
		
		description:SetText(sDesc)
		--Description:SetText(line:GetID().." | "..line:GetValue(2).." | "..line:GetValue(3))
		PassiveList:ClearSelection()
		PassiveList:SelectItem(line)
	end
	
	parent:AddItem( PassiveList )
	
	return PassiveList
end

function CreateActiveTab(parent)
	local ActiveTab = vgui.Create( "DPanelList", parent )
	ActiveTab:SetPos( 25,25 )
	ActiveTab:SetSize( 300, 200 )
	ActiveTab:SetSpacing( 5 ) -- Spacing between items
	ActiveTab:EnableHorizontal( false ) -- Only vertical items
	ActiveTab:EnableVerticalScrollbar( true ) -- Allow scrollbar if you exceed the Y axis
	
	return ActiveTab
end

function CreateActiveList(parent, description)
	local ActiveList = vgui.Create( "DListView", parent )
	ActiveList:SetSize(600, 300)
	ActiveList:SetMultiSelect( false )
	ActiveList:AddColumn( "Skill" )
	ActiveList:AddColumn( "Current Level" )
	ActiveList:AddColumn( "Upgrade Cost" )
	
	// Add skills to list
	for k, _ in ipairs(g_SkillList["active"]) do
		local cost = "Maxed"
		
		if(g_SkillList["active"][k]["level"] < #g_SkillList["active"][k]) then
			cost = g_SkillList["active"][k][g_SkillList["active"][k]["level"] + 1]["cost"]
		end
		
		ActiveList:AddLine( g_SkillList["active"][k]["name"], g_SkillList["active"][k]["level"], tostring(cost) )
	end
	
	ActiveList:SelectFirstItem()
	
	ActiveList.OnClickLine = function(parent, line, isselected)
		local sDesc = "Maxed"
		
		if(g_SkillList["active"][line:GetID()]["level"] < #g_SkillList["active"][line:GetID()]) then
			sDesc = "Next level: "..g_SkillList["active"][line:GetID()][g_SkillList["active"][line:GetID()]["level"] + 1]["description"]
		else
			sDesc = "Maxed out: "..g_SkillList["active"][line:GetID()][g_SkillList["active"][line:GetID()]["level"]]["description"]
		end
		
		description:SetText(sDesc)
		ActiveList:ClearSelection()
		ActiveList:SelectItem(line)
	end
	
	parent:AddItem( ActiveList )
	
	return ActiveList
end

function CreateBasicTab(parent)
	local BasicTab = vgui.Create( "DPanelList", parent )
	BasicTab:SetPos( 25,25 )
	BasicTab:SetSize( 300, 200 )
	BasicTab:SetSpacing( 5 ) -- Spacing between items
	BasicTab:EnableHorizontal( false ) -- Only vertical items
	BasicTab:EnableVerticalScrollbar( true ) -- Allow scrollbar if you exceed the Y axis
	
	return BasicTab
end

function CreateBasicList(parent, description)
	local BasicList = vgui.Create( "DListView", parent )
	BasicList:SetSize(600, 300)
	BasicList:SetMultiSelect( false )
	BasicList:AddColumn( "Skill" )
	BasicList:AddColumn( "Points added" )
	
	// Add Skills to list
	for k, _ in ipairs(g_SkillList["basic"]) do
		BasicList:AddLine( g_SkillList["basic"][k]["name"], g_SkillList["basic"][k]["stats"] )
	end
	BasicList:SelectFirstItem()
	
	BasicList.OnClickLine = function(parent, line, isselected)
		sDesc = g_SkillList["basic"][line:GetID()]["description"]
		
		description:SetText(sDesc)
		BasicList:ClearSelection()
		BasicList:SelectItem(line)
	end
	
	parent:AddItem(BasicList)
	
	return BasicList
end

-- Draw box on screen
function GM:HUDPaintBackground() --could use HUDPaintBackground
	if(LocalPlayer():Alive()) then
		DrawTopLeftHud() -- Create items in top left of hud
		
		DrawEXPBar() -- Create EXP bar
		
		DrawMobInfo() -- Draw selected mob's health bar and name
	end
end

function DrawTopLeftHud()
	// Draw border around avatar
	surface.SetDrawColor( 0, 0, 0, 255)
	surface.DrawRect(8 , 6, 68, 68)
	
	// Draw class/level
	draw.SimpleText("Level 10 Engineer", "SPF_N", 82, 33, Color(255, 255, 255, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER);
	
	// Draw health bar			
	surface.SetDrawColor(0, 0, 0, 255);
	surface.DrawOutlinedRect(80, 6, 194, 18);
	
	surface.SetDrawColor(255, 0, 0, 255);
	surface.DrawRect(81, 7, (LocalPlayer():Health() / 100) * 192, 16);
	
	draw.SimpleText(LocalPlayer():Health().." / ".. 100, "SPF_N", 177, 14, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
end

function DrawEXPBar()
	// Draw EXP bar
	local reqExp = 1234;
	local plyExp = 729;
	
	surface.SetDrawColor( 0, 0, 0, 255);
	surface.DrawRect(0 , ScrH() - 9, ScrW(), 9);
	
	surface.SetDrawColor( 0, 255, 0, 255);
	surface.DrawRect(0 , ScrH() - 7, (plyExp / reqExp) * ScrW(), 7);
	
	surface.SetDrawColor( 34, 139, 34, 255);
	surface.DrawRect((plyExp / reqExp) * ScrW() , ScrH() - 7, ScrW() - ((plyExp / reqExp) * ScrW()), 7);
	
	surface.SetDrawColor(0, 0, 0, 255);
	
	for i=1, 9 do
		surface.DrawRect(ScrW() * (i/10) - 1, ScrH() - 7, 2, 7);
	end
	
	draw.SimpleText(string.format("%.1f", (plyExp / reqExp) * 100) .."%", "SPF_N", ScrW() / 2, ScrH() - 20, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
end

function DrawMobInfo()
	// Draw current monster health bar
	local npc = LocalPlayer():GetNetworkedEntity("SelectedNPC");
	
	if(npc:IsValid()) then
		draw.SimpleText(npc:Health().." / "..LocalPlayer():GetNetworkedInt("MaxHealth"), "SPF_N", ScrW() / 2, 25, Color(0, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
		
		local NPCName = EntityClassToName(LocalPlayer():GetNetworkedString("NPCName"));
		draw.SimpleText(NPCName, "SPF_N", ScrW() / 2, 10, Color(0, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER);
	end
end

function GM:HUDShouldDraw( name )
    if ( name == "CHudHealth" or name == "CHudAmmo") then
        return false;
    end
	
    return true;
end

function GM:DrawDeathNotice(x, y)
	return
end

/*
hook.Add("Think", "BM_Clients_Key", function()
	local isF1Down = input.IsKeyDown( KEY_F1 )

	if(isF1Down and !g_IsF1Down) then
		OpenStatsMenu()
	end
	
	g_IsF1Down = isF1Down
end)
*/

function EntityClassToName(class)
	if(class == "npc_zombie") then
		return "Zombie";
	elseif(class == "npc_dog") then
		return "Dog";
	else
		return class;
	end;
end

/*
local matOutline = CreateMaterial( "BlackOutline", "UnlitGeneric", { [ "$basetexture" ] = "vgui/black" } )
 
hook.Add( "PostDrawOpaqueRenderables", "PostDrawing", function()
	cam.Start3D( EyePos(), EyeAngles() )
 
	// First we clear the stencil and enable the stencil buffer
	render.ClearStencil()
	render.SetStencilEnable( true )
 
	// First we set every pixel with the prop + outline to 1
	render.SetStencilFailOperation( STENCILOPERATION_KEEP )
	render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
	render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
	render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
	render.SetStencilReferenceValue( 1 )
 
	for _, ent in pairs( ents.FindByClass( "npc_dog_inv" ) ) do
		ent:SetModelScale( 1.1, 0)
		--print( ent:GetModelScale() )
		ent:DrawModel()
		ent:SetModelScale( 1, 0 )
	end
 
	// Now we set every pixel with only the prop to 2
	render.SetStencilReferenceValue( 2 )
 
	for _, ent in pairs( ents.FindByClass( "npc_dog_inv" ) ) do
		ent:DrawModel()
	end
 
	// Now we only draw the pixels with a value of 1 black, which are the pixels with only the outline
	render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
	render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
	render.SetStencilReferenceValue( 1 )
 
	render.SetMaterial( matOutline )
	render.DrawScreenQuad()
 
	// Disable the stencil buffer again
	render.SetStencilEnable( false )
 
	cam.End3D()
end )
*/