AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_rpg.lua" )

include( "shared.lua" )

util.AddNetworkString( "PlayerSpawned" )
util.AddNetworkString( "PlayerDied" )
util.AddNetworkString( "PlayerAuthed" )
--util.AddNetworkString( "PlayerInitialSpawn" )

function GM:PlayerSpawn( ply )
	net.Start( "PlayerSpawned" )
	net.Send( ply )
	
	ply:Give( "weapon_crossbow" )
	ply:Give( "weapon_crowbar" )
	
	ply:SetModel( "models/alyx.mdl" )
end

function GM:PlayerDeath( victim, inflictor, attacker )
	net.Start( "PlayerDied" )
	net.Send( victim )
end

function GM:PlayerAuthed( ply )
	net.Start( "PlayerAuthed" )
	net.WriteString( ply:SteamID64( ) )
	net.Send( ply )
end