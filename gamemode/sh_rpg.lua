SKILL_BASIC_WEAPONSPEED  = 1
SKILL_BASIC_HEALTHBONUS  = 2
SKILL_BASIC_DAMAGEREDUC  = 3
SKILL_BASIC_DAMAGEBONUS  = 4
SKILL_BASIC_REGENERATION = 5

SKILL_ACTIVE_SENTRY     = 1
SKILL_ACTIVE_RESURRECT  = 2
SKILL_ACTIVE_VORTEX     = 3
SKILL_ACTIVE_LIGHTNING  = 4
SKILL_ACTIVE_MAGICMAKER = 5

SKILL_PASSIVE_JUMPHEIGHT  = 1
SKILL_PASSIVE_VAMPIRISM   = 2
SKILL_PASSIVE_IRONLEGS    = 3
SKILL_PASSIVE_QUICKFOOT   = 4
SKILL_PASSIVE_RESUPPLY    = 5
SKILL_PASSIVE_AUTOHOP     = 6
SKILL_PASSIVE_REDEMPTION  = 7
SKILL_PASSIVE_ULTIMA      = 8
SKILL_PASSIVE_EQUILIBRIUM = 9

concommand.Add("levelup", function( ply, command, arguments )
	net.Start( "LevelUp" )
	net.WriteString( "basic" )
	net.WriteInt( SKILL_BASIC_WEAPONSPEED, 32 )
	net.WriteInt( 10,  32)
	net.Send( ply )
	
	if(#arguments == 2) then
		if(arguments[1] == "basic") then
			
		elseif(arguments[1] == "passive" or arguments[1] == "active") then
			
		end
	end
end)

function InitializeRPG()
	if(SERVER) then
		util.AddNetworkString( "LevelUp" )
		
		if(!sql.TableExists("inv_stats")) then
			sql.Query("CREATE TABLE inv_stats (unique_id varchar(255), lvl int, exp int, stats int, wepspeed int, hpbonus int, dmgreduc int, dmgbonus int, \
				regen int, sentry int, resurrect int, vortex int, lightning int, magicmaker int, jumpheight int, vamp int, ironlegs int, quickfoot int, resupply int, autohop int, \
				redemption int, ultima int, equilibrium int)")
		end
	elseif(CLIENT) then
		if(!sql.TableExists("inv_settings")) then
			sql.Query("CREATE TABLE inv_settings (unique_id varchar(255), use_auto int)")
		end
	end
end

hook.Add( "PlayerAuthed", "Authed", function(ply)
	if(SERVER) then
		ply.SkillList =
		{
			["basic"] =
			{
				[SKILL_BASIC_WEAPONSPEED] =
				{
					["name"]        = "Weapon Speed",
					["description"] = "Increases your weapon speed by 1% for every stat put in.",
					["stats"]       = 0
				},
				[SKILL_BASIC_HEALTHBONUS] =
				{
					["name"]        = "Health Bonus",
					["description"] = "Increases your max health by 1 for every stat put in.",
					["stats"]       = 0
				},
				[SKILL_BASIC_DAMAGEREDUC] =
				{
					["name"]        = "Damage Reduction",
					["description"] = "Increases your damage reduction by 0.5% for every stat put in.",
					["stats"]       = 0
				},
				[SKILL_BASIC_DAMAGEBONUS] =
				{
					["name"]        = "Damage Bonus",
					["description"] = "Increases your damage by 0.5% for every stat put in.",
					["stats"]       = 0
				},
				[SKILL_BASIC_REGENERATION] =
				{
					["name"]        = "Regeneration",
					["description"] = "Increases your health by 1 every 5 seconds for every stat put in.",
					["stats"]       = 0
				}
			},
			["active"] =
			{
				[SKILL_ACTIVE_SENTRY] =
				{
					["name"]  = "Sentry",
					["level"] = 0,
					[1] =
					{
						["description"] = "Spawns a sentry that shoots mobs for you. HP: 150, DB: 0%.",
						["cost"]        = 100
					},
					[2] =
					{
						["description"] = "Spawns a sentry that shoots mobs for you. HP: 370, DB: 100%.",
						["cost"]        = 400
					},
					[3] =
					{
						["description"] = "Spawn up to 2 sentries that shoot mobs for you. HP: 1000, DB: 300%.",
						["cost"]        = 1000
					},
					[4] =
					{
						["description"] = "Spawn up to 2 sentries that shoot mobs for you and also do AoE damage. HP: 1400, DB: 500%.",
						["cost"]        = 5000
					}
				},
				[SKILL_ACTIVE_RESURRECT] =
				{
					["name"]  = "Resurrect",
					["level"] = 0,
					[1] =
					{
						["description"] = "Allows you to resurrect another player (once per round).",
						["cost"]        = 100
					}
				},
				[SKILL_ACTIVE_VORTEX] =
				{
					["name"]  = "Vortex",
					["level"] = 0,
					[1] =
					{
						["description"] = "Creates a vortex where you're looking.",
						["cost"]        = 100
					}
				},
				[SKILL_ACTIVE_LIGHTNING] =
				{
					["name"]  = "Lightning Rod",
					["level"] = 0,
					[1] =
					{
						["description"] = "You body becomes a lightning rod and shocks nearby mobs.",
						["cost"]        = 100
					}
				},
				[SKILL_ACTIVE_MAGICMAKER] =
				{
					["name"]  = "Magic Weapon Maker",
					["level"] = 0,
					[1] =
					{
						["description"] = "You can give weapons magic properties.",
						["cost"]        = 100
					}
				}
			},
			["passive"] =
			{
				[SKILL_PASSIVE_JUMPHEIGHT] =
				{
					["name"]  = "Jump Height",
					["level"] = 0,
					[1] =
					{
						["description"] = "Increases your jump height by 100%.",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "Increases your jump height by 100% and allows you to double jump.",
						["cost"]        = 100
					},
					[3] =
					{
						["description"] = "Increases your jump height by 200% and allows you to double jump.",
						["cost"]        = 250
					},
					[4] =
					{
						["description"] = "Increases your jump height by 200% and allows you to jump infinitely.",
						["cost"]        = 1000
					}
				},
				[SKILL_PASSIVE_VAMPIRISM] =
				{
					["name"]  = "Vampirism",
					["level"] = 0,
					[1] =
					{
						["description"] = "5% of the damage you cause is given back to you in health.",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "10% of the damage you cause is given back to you in health.",
						["cost"]        = 250
					},
					[3] =
					{
						["description"] = "30% of the damage you cause is given back to you in health.",
						["cost"]        = 900
					},
					[4] =
					{
						["description"] = "60% of the damage you cause is given back to you in health.",
						["cost"]        = 1500
					},
					[5] =
					{
						["description"] = "100% of the damage you cause is given back to you in health.",
						["cost"]        = 5000
					}
					
				},
				[SKILL_PASSIVE_IRONLEGS] =
				{
					["name"]  = "Iron Legs",
					["level"] = 0,
					[1] =
					{
						["description"] = "Reduces fall damage by 10%",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "Reduces fall damage by 20%",
						["cost"]        = 60
					},
					[3] =
					{
						["description"] = "Reduces fall damage by 40%",
						["cost"]        = 135
					},
					[4] =
					{
						["description"] = "Reducdes fall damage by 80%",
						["cost"]        = 300
					},
					[5] =
					{
						["description"] = "Reduces fall damage by 100%",
						["cost"]        = 1000
					}
				},
				[SKILL_PASSIVE_QUICKFOOT] =
				{
					["name"]  = "Quickfoot",
					["level"] = 0,
					[1] =
					{
						["description"] = "Increases your movement speed by 5%",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "Increases your movement speed by 10%",
						["cost"]        = 100
					},
					[3] =
					{
						["description"] = "Increases your movement speed by 20%",
						["cost"]        = 200
					},
					[4] =
					{
						["description"] = "Increases your movement speed by 50%",
						["cost"]        = 500
					},
					[5] =
					{
						["description"] = "Increases your movement speed by 100%",
						["cost"]        = 1000
					}
				},
				[SKILL_PASSIVE_RESUPPLY] =
				{
					["name"]  = "Resupply",
					["level"] = 0,
					[1] =
					{
						["description"] = "Regenerates 1 ammo per second.",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "Regenerates 2 ammo per second.",
						["cost"]        = 50
					},
					[3] =
					{
						["description"] = "Regenerates 4 ammo per second.",
						["cost"]        = 100
					},
					[4] =
					{
						["description"] = "Regenerates 8 ammo per second.",
						["cost"]        = 300
					},
					[5] =
					{
						["description"] = "You gain infinite ammo.",
						["cost"]        = 2000
					}
				},
				[SKILL_PASSIVE_AUTOHOP] =
				{
					["name"]  = "Autobhop",
					["level"] = 0,
					[1] =
					{
						["description"] = "You can gain perfect jumps by holding your jump button down.",
						["cost"]        = 500
					}
				},
				[SKILL_PASSIVE_REDEMPTION] =
				{
					["name"]  = "Redemption",
					["level"] = 0,
					[1] =
					{
						["description"] = "You are granted a second chance at life per wave.",
						["cost"]        = 500
					}
				},
				[SKILL_PASSIVE_ULTIMA] =
				{
					["name"]  = "Ultima",
					["level"] = 0,
					[1] =
					{
						["description"] = "Causes a big explosion upon death.",
						["cost"]        = 500
					}
				},
				[SKILL_PASSIVE_EQUILIBRIUM] =
				{
					["name"]  = "Equilibrium",
					["level"] = 0,
					[1] =
					{
						["description"] = "Deals 5% of the damage to enemies that inflict damage on you.",
						["cost"]        = 25
					},
					[2] =
					{
						["description"] = "Deals 10% of the damage to enemies that inflict damage on you.",
						["cost"]        = 100
					},
					[3] =
					{
						["description"] = "Deals 30% of the damage to enemies that inflict damage on you.",
						["cost"]        = 300
					},
					[4] =
					{
						["description"] = "Deals 50% of the damage to enemies that inflict damage on you.",
						["cost"]        = 1000
					}
				}
			}
		}
		
		if(!CreatePlayer(ply)) then
		
		else
			ply.Level = 1
			ply.Experience = 0
		end
	end
end)

function CreatePlayer(ply)
	local result = sql.QueryValue("SELECT unique_id FROM inv_stats WHERE unique_id = '"..ply:SteamID().."'")
	
	if(!result) then
		sql.Query("INSERT INTO inv_stats ('unique_id', 'lvl') VALUES('"..ply:SteamID().."', '1')")
		
		return true
	end
	
	return false
end