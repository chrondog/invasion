GM.Name 		= "Invasion"
GM.Author 		= "blacky"
GM.Email 		= "ryanc@contractor.net"
GM.Website 		= "http://steamcommunity.com/id/blaackyy/"
GM.TeamBased 	= false

DeriveGamemode("base") 
DEFINE_BASECLASS("gamemode_base")

include( "sh_rpg.lua" )

// The number of living mobs
g_MobCount		  = 0

// How long in seconds for the invasion to start
g_DelayAfterStart = 5

// The countdown starting number
g_WaveCountdown   = 1

// true if a wave is in progress
g_IsWaveHappening = false

// The current wave number
g_CurrentWave     = 0

/* 
 * The mob spawning script --
 * "killsrequired" is how many kills are needed that wave
 * "maxmobcount" is how many mobs are allowed to be alive during the wave
 * "class" is the classname of the mob that will spawn
 * "chance" is the pct. chance the movie will spawn
 * "attempts" is how many tries the mob will attempt to spawn, allowing for multiple to spawn
*/
g_SpawnScript = 
{ 
	[1] = 
	{	["killsrequired"] = 35,
		["maxmobcount"]   = 20,
		[1] =
		{
			["class"]    = "npc_headcrab",
			["chance"]   = 0.8,
			["attempts"] = 3
		}, 
		[2] =
		{
			["class"]    = "npc_dog_inv",
			["chance"]   = 0.2,
			["attempts"] = 2
		},
		[3] =
		{
			["class"]    = "npc_rollermine",
			["chance"]   = 0.25,
			["attempts"] = 4
		}
	},
	[2] = 
	{
		["killsrequired"] = 30,
		["maxmobcount"]   = 20,
		[1] =
		{
			["class"]    = "npc_vortigaunt",
			["chance"]   = 0.3,
			["attempts"] = 2
		}, 
		[2] =
		{
			["class"]    = "npc_zombie",
			["chance"]   = 0.7,
			["attempts"] = 4
		}
	},
	[3] = 
	{
		["killsrequired"] = 30,
		["maxmobcount"]   = 20,
		[1] =
		{
			["class"]    = "npc_vortigaunt",
			["chance"]   = 0.3,
			["attempts"] = 2
		}, 
		[2] =
		{
			["class"]    = "npc_zombie",
			["chance"]   = 0.7,
			["attempts"] = 4
		}
	}
}

function GM:Initialize()
	// Start the invasion at the beginning of the map
	timer.Create("StartInvasion", g_DelayAfterStart, 1, Timer_StartInvasion);
	
	// Start the loop for mobs to spawn
	timer.Create( "SpawnMobs", 5, 0, Timer_SpawnMobs )
	
	// Precache custom sounds
	util.PrecacheSound("next_wave_in.wav")
	for i=1, 10 do
		util.PrecacheSound(tostring(i)..".wav")
	end
	
	InitializeRPG()
	
end

function GM:OnNPCKilled( npc, attacker, inflictor )
	if( g_IsWaveHappening == true and 0 < g_CurrentWave and g_CurrentWave <= #g_SpawnScript ) then
		
		g_SpawnScript[g_CurrentWave]["killsrequired"] = g_SpawnScript[g_CurrentWave]["killsrequired"] - 1
		
		g_MobCount = g_MobCount - 1
		
		if( g_SpawnScript[g_CurrentWave]["killsrequired"] <= 0 ) then
			// Stop the current wave
			g_IsWaveHappening = false
			
			// Start the next wave
			StartNextWave( )
			
			// Clear the map of any already existing mobs
			ClearMobs( )
		end
	end
end

function Timer_StartInvasion( )
	// Start the first wave
	StartNextWave( )
end

function StartNextWave( )
	if(SERVER) then
		local countdown = g_WaveCountdown
		
		// Play the "next wave in.." sound
		for k, v in pairs( player.GetAll( ) ) do
			v:PrintMessage( HUD_PRINTCENTER, "Next wave in.. " )
			v:EmitSound("next_wave_in.wav")
		end
		
		// Delay the countdown for the "next wave in.." sound to finish
		timer.Create( "NextWaveIn", 1.2, 1, function( )
			// Start countdown
			timer.Create( "Countdown", 1, g_WaveCountdown + 1, function( )
				if( countdown > 0 ) then
					// Message the current countdown to all players
						for k, v in pairs( player.GetAll( ) ) do
							if(countdown <= 10) then
								v:EmitSound(tostring(countdown)..".wav")
							end
							
							v:PrintMessage( HUD_PRINTCENTER, countdown )
						end
						countdown = countdown - 1
				else
					// Allow mobs to spawn
					g_IsWaveHappening = true
					
					// Set it to the next wave
					g_CurrentWave     = g_CurrentWave + 1
					
					// Reset mob count
					g_MobCount		  = 0
					
					// Immediately spawn some initial mobs
					Timer_SpawnMobs( )
				end
			end)
		end)
	end
end

function GM:InitPostEntity( )
	g_SpawnList = ents.FindByClass( "info_player_start" )
end

function GM:PlayerNoClip( ply )
	return false
end

function GM:ShouldCollide( ent1, ent2 )
	if( string.StartWith( ent1:GetClass(), "npc_" ) and string.StartWith( ent2:GetClass( ), "npc_" ) ) then
		return false
	else
		return true
	end
end

function Timer_SpawnMobs( )
	if( SERVER and g_IsWaveHappening and 0 < g_CurrentWave and g_CurrentWave <= #g_SpawnScript ) then
		for k, v in ipairs( g_SpawnScript[g_CurrentWave] ) do
			for i=1, g_SpawnScript[g_CurrentWave][k]["attempts"] do
				if( math.random( ) <= g_SpawnScript[g_CurrentWave][k]["chance"] ) then
					if( g_MobCount <= g_SpawnScript[g_CurrentWave]["maxmobcount"] ) then
						SpawnMob( g_SpawnScript[g_CurrentWave][k]["class"] )
					end
				end
			end
		end
	end
end

function SpawnMob( class )
	// Loop through spawns in a random order
	for k, v in RandomPairs( g_SpawnList ) do
		// Find every entity near the random spawn
		local nearplayers = ents.FindInSphere( v:GetPos( ), 1000 )
		
		// Loop through entities near random spawn in a random order
		for i, j in RandomPairs( nearplayers ) do
			// If the entity is a player, spawn the NPC at the current random spawn
			if(j:IsPlayer()) then
				local ent = ents.Create(class);
				ent:SetPos(v:GetPos());
				ent:Spawn()
				ent:SetCustomCollisionCheck(true)
				
				// Make standard NPCs hate players more than any other mob
				if(ent:IsNPC()) then
					ent:ClearEnemyMemory();
					ent:AddRelationship("player D_HT 99");
					ent:SetNPCState(NPC_STATE_COMBAT);
					ent:UseAssaultBehavior();
				end
				
				if(ent:IsValid()) then
					g_MobCount = g_MobCount + 1
				end
				
				return
			end
		end
	end
end

function GM:OnEntityCreated( ent )
	if (ent:GetClass()=="class C_ClientRagdoll") then
		ent:Remove()
	end
end

function GM:PlayerTick( ply, cmd )
	if(SERVER) then
		local npc = ply:GetEyeTrace().Entity;

		if( npc:IsValid( ) ) then
			if( string.StartWith( npc:GetClass( ), "npc_" ) ) then
				if( ply.SelectedNPC != npc ) then
					ply.SelectedNPC = npc;
					ply:SetNetworkedEntity( "SelectedNPC", ply.SelectedNPC );
					ply:SetNetworkedInt( "MaxHealth", ply.SelectedNPC:GetMaxHealth( ) );
					ply:SetNetworkedString( "NPCName", ply.SelectedNPC:GetClass( ) );
				end
			end
		end
	end
end

function ClearMobs()
	for k, v in ipairs(ents.GetAll()) do
		if(string.StartWith(v:GetClass(), "npc_")) then
			v:Remove()
		end
	end
end